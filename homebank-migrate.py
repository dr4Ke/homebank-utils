#!/usr/bin/env python3
#
import sys
import argparse
import xml.etree.ElementTree  as ET
import defusedxml.ElementTree as DET

EMPTY_HOMEBANK = """<?xml version="1.0"?>
<homebank v="1.3999999999999999" d="050501">
<properties title="Unknown" curr="1" auto_smode="1" auto_weekday="1"/>
<cur key="1" flags="0" iso="EUR" name="Euro" symb="€" syprf="0" dchar="," gchar=" " frac="2" rate="0" mdate="0"/>
</homebank>
"""

ELTYPES = {
    'payee':      'pay',
    'category':   'cat',
    'assignment': 'asg'
}

argparser = argparse.ArgumentParser(description='Import/Export homebank data')
argparser.add_argument('-i', '--input', metavar='INPUTFILE', type=str,
                       action='store', default=sys.stdin,
                       help='Input file')
argparser.add_argument('-o', '--output', metavar='OUTPUTFILE', type=str,
                       action='store', default=sys.stdout,
                       help='Output file')
argparser.add_argument('-d', '--debug', dest='DEBUG',
                       action='store_true', default=False,
                       help='Activate debug output')
argparser.add_argument('-T', '--trace', dest='TRACE',
                       action='store_true', default=False,
                       help='Activate debug output')
args = argparser.parse_args()

def _print(msg, level='INFO'):
    if level == 'DEBUG' and not args.DEBUG:
        return
    if level == 'TRACE' and not args.TRACE:
        return
    if msg[-1] != '\n':
        msg += '\n'
    sys.stderr.write(level + ': ' + msg)

def _get_payees(xmltree):
    payees = {}
    for tag in xmltree.findall('pay'):
        payees.update({tag.get('key'): tag.get('name')})
    return payees

def _get_categories(xmltree):
    categories = {}
    for tag in xmltree.findall('cat'):
        category = {
            tag.get('key'): {
                'name': tag.get('name'),
                'parent': tag.get('parent'),
                'flags': tag.get('flags'),
            }
        }
        categories.update(category)
    return categories

def _get_assignments(xmltree, payees, categories):
    assignments = {}
    for tag in xmltree.findall('asg'):
        assignment = {
            'name': tag.get('name'),
            'pos': tag.get('pos'),
            'flags': tag.get('flags'),
            'notes': tag.get('notes'),
            'payee': tag.get('payee'),
            'category': tag.get('category'),
            'paymode': tag.get('paymode'),
        }
        if tag.get('payee'):
            assignment.update({
                'payee': payees[tag.get('payee')],
            })
        if tag.get('category'):
            assignment.update({
                'category': {
                    'name': categories[tag.get('category')]['name'],
                    'parent': categories[categories[tag.get('category')].get('parent')]['name'],
                }
            })
        assignments.update({tag.get('key'): assignment})
    return assignments

def _get_payee_index(payees, wanted):
    _print('Searching payee index for {}'.format(wanted), 'DEBUG')
    wanted_index = None
    for index, payee in payees.items():
        _print('index: {}, payee: {}'.format(index, payee), 'TRACE')
        if payee == wanted:
            wanted_index = index
    _print('Index found: {}'.format(str(wanted_index)), 'DEBUG')
    return wanted_index

def _get_category_index(categories, wanted):
    _print('Searching category index for {}::{}'.format(wanted.get('parent'), wanted.get('name')), 'DEBUG')
    wanted_index = None
    for index, cat in categories.items():
        _print('index: {}, cat: {}'.format(index, cat), 'TRACE')
        if cat.get('name') == wanted.get('name'):
            if wanted.get('parent') is None and cat.get('parent') is None:
                wanted_index = index
            elif wanted.get('parent') and cat.get('parent') and categories[cat.get('parent')].get('name') == wanted.get('parent'):
                wanted_index = index
    _print('Index found: {}'.format(str(wanted_index)), 'DEBUG')
    return wanted_index

def _get_next_index(_dict):
    if _dict == {}:
        return 1
    else:
        return int(max(_dict.keys(), key=lambda k: int(k))) + 1

def _make_category_update(_cat):
    cat_index = _get_next_index(outcategories)
    cat_update = {}
    if not _cat.get('parent') in cat_names:
        cat_update.update({
            str(cat_index): incategories[_get_category_index(incategories, {'name': _cat.get('parent')})]
        })
        parent_key = str(cat_index)
        cat_index += 1
        cat_names.append(_cat.get('parent'))
    else:
        parent_key = _get_category_index(outcategories, {'name': _cat.get('parent')})
    newcat = incategories[_get_category_index(incategories, _cat)]
    _print('New category: {}'.format(str(newcat)), 'DEBUG')
    newcat.update({'parent': parent_key})
    _print('New category: {}'.format(str(newcat)), 'DEBUG')
    cat_update.update({
        str(cat_index): newcat
    })
    cat_names.append(_cat.get('parent') + '::' + _cat.get('name'))
    _print('Category update dict: {}'.format(str(cat_update)), 'DEBUG')
    return cat_update

def _get_element(_tree, _type, _key):
    _elt = None
    for _tag in _tree.findall(_type):
        if _tag.get('key') == _key:
            _elt = _tag
            break
    return _elt

def _add_element(_tree, _type, _elts):
    _print('Add {} element {}'.format(_type, _elts), 'DEBUG')
    for _key, _elt in _elts.items():
        if _get_element(_tree, _type, _key):
            return
        if _type == 'payee':
            _elt = {'name': _elt}
        elif _type == 'assignment':
            if _elt['payee']:
                _elt.update({'payee': _get_payee_index(outpayees, _elt['payee'])})
            else:
                del _elt['payee']
            if _elt['category']:
                _elt.update({'category': _get_category_index(outcategories, _elt['category'])})
            else:
                del _elt['category']
            if _elt['paymode'] is None:
                del _elt['paymode']
        elif _type == 'category':
            if _elt['flags'] is None:
                del _elt['flags']
            if _elt['parent'] is None:
                del _elt['parent']
        _elt.update({'key': _key})
        _newelt = ET.SubElement(_tree, ELTYPES[_type], attrib=_elt)
        _print('{} element added: {}'.format(ELTYPES[_type], str(_newelt.items())), 'TRACE')

_print(str(args), 'DEBUG')

_print('Parsing input')
inxml = DET.parse(args.input)
inhb = inxml.getroot()

inpayees = _get_payees(inxml)
_print('Input payees: ' + str(inpayees), 'DEBUG')

_print('Get categories')
incategories = _get_categories(inhb)
_print('Input categories: ' + str(incategories), 'DEBUG')

_print('Get assignments')
inassignments = _get_assignments(inhb, inpayees, incategories)
_print('Input assignments: ' + str(inassignments), 'DEBUG')

if args.output == sys.stdout:
    outhb = DET.fromstring(EMPTY_HOMEBANK)
    outxml = ET.ElementTree(outhb)
else:
    _print('Parsing output file')
    outxml = DET.parse(args.output)
    outhb = outxml.getroot()

outpayees = _get_payees(outhb)
_print('Output payees: ' + str(outpayees), 'DEBUG')
outcategories = _get_categories(outhb)
_print('Output categories: ' + str(outcategories), 'DEBUG')
outassignments = _get_assignments(outhb, outpayees, outcategories)
_print('Output assignments: ' + str(outassignments), 'DEBUG')

asg_names = []
pay_names = outpayees.values()
cat_names = []
asg_index = _get_next_index(outassignments)
pay_index = _get_next_index(outpayees)
_print('out index: ' + str(asg_index))
for item in outassignments.values():
    asg_names.append(item['name'])
for item in outcategories.values():
    if item.get('parent'):
        cat_names.append(outcategories[item.get('parent')]['name'] + '::' +
                         item.get('name'))
    else:
        cat_names.append(item.get('name'))

_print('Add assignments to output values')
for key, asg in sorted(inassignments.items(), key=lambda item: int(item[1]['pos'])):
    _print(asg['pos'] + ': ' + key + ': ' + str(asg), 'TRACE')
    if asg['name'] in asg_names:
        continue
    if asg['payee'] and not asg['payee'] in pay_names:
        outpayees.update({str(pay_index): asg['payee']})
        _add_element(outhb, 'payee', {str(pay_index): asg['payee']})
        pay_index += 1
    asg_cat = asg.get('category')
    if asg_cat and not asg_cat.get('parent') + '::' + asg_cat.get('name') in cat_names:
        category_update = _make_category_update(asg_cat)
        outcategories.update(category_update)
        _add_element(outhb, 'category', category_update)
    asg.update({'pos': str(asg_index)})
    outassignments.update({
        str(asg_index): asg
    })
    _add_element(outhb, 'assignment', {str(asg_index): asg})
    asg_index += 1
_print('Output assignments: ' + str(outassignments), 'DEBUG')
_print('Output payees: ' + str(outpayees), 'DEBUG')
_print('Output categories: ' + str(outcategories), 'DEBUG')

_print('Write output file')
if args.output == sys.stdout:
    ET.dump(outxml)
else:
    outxml.write(args.output)
