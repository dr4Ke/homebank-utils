# Homebank utility scripts

## Migrate data

`homebank-migrate.py` is a script that can migrate data from a homebank file to
another. Currently only able to move assignments, along with needed payees and
categories.

Warning: This is a work in progress.
